class Tasks < ActiveRecord::Base
	validates :title, presence: true
	validates :description, presence: true
	enum status: [ :Open, :in_progress, :Fixed, :Verified ]
end
