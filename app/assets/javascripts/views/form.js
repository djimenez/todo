app.views.form = Backbone.View.extend({
	id: false,
	events: {
		'click button.save': 'save',
        'click button.cancel': 'cancel',
	},
    initialize: function() {
        this.render();
    },
    render: function(id) {
        var template, html = $("#tpl-form").html();
        if(typeof id == 'undefined') {
        	this.id = false;
        	template = _.template(html, { title: "" , description: "" , status:null});
        } else {
        	this.id = parseInt(id);
        	this.todoEdit = this.model.get({ id:this.id });
        	template = _.template($("#tpl-form").html(), {
        		title: this.todoEdit.get("title"),
                description: this.todoEdit.get("description"),
                status: this.todoEdit.get("status")
        	});
        }
        this.$el.html(template);
        this.$el.find(".title").focus();
        this.delegateEvents();
        return this;
    },
    save: function(e) {
    	e.preventDefault();
        var isOk = false;
        var thisSave = this;
    	var title = this.$el.find(".title").val();
        var description = this.$el.find(".description").val();
        var status = this.$el.find(".status").val();
    	if(this.id !== false) {
    		this.todoEdit.save({ title:title, description:description, status:status });
            this.todoEdit.set({ status: app.stats[status] });
            app.todos.fetch({ remove: true, add : true });
            isOk = true;        
    	} else {
            var newTask = new app.models.ToDo;
    		newTask.save({ title:title, description:description, status:status },{
                            success: function(data){
                                if(data.get('error') == 'true'){
                                    app.errors(data);
                                    return;
                                }
                                isOk = true;
                                thisSave.trigger("completed");
                                $('.wrap-form').remove(); 
                                app.todos.fetch({ remove: true, add : true });
                            }
                         });

    	}
        if(isOk == true){
            this.$el.find('.wrap-form').remove();   
            this.trigger("completed"); 
        }
   	
    },
    cancel: function(e) {
        e.preventDefault();
        this.$el.find('.wrap-form').remove();
        this.trigger("completed"); 
    }
});