app.views.list = Backbone.View.extend({
	events: {
	},
	initialize: function() {
		var handler = _.bind(this.render, this);
		this.model.bind('change', handler);
		this.model.bind('add', handler);
		this.model.bind('remove', handler);
	},
    render: function() {

		var dragmodel = this.model;

		app.stats.forEach(function(element,index){
			var modelTemp = dragmodel.where({status:element});
			var html = '';
			var template = '';
			_.each(modelTemp,function(todo, index) {
				template = _.template($("#tpl-list-item").html());
				html += template({ 
					title: todo.get("title"),
		            description: todo.get("description"),
		            status: todo.get("status"),
					id: todo.get("id")
				});
				;
			});
			$('.'+element).html(html)
		});

    	this.delegateEvents();

    	$(".task").draggable({
            start: function(e, ui) {
                $(ui.helper).addClass("ui-draggable-topic");
            },
            cursor: 'pointer',
            stack: 	'.task',
            revert: true
        });

    	$(".task-bucket").droppable({
            accept: '.task',
            drop: function(e,ui){
				var status = $(e.target).data('status');
				var id = $(ui.draggable).data('index');
				ui.draggable.position( { of: $(this) } );
				ui.draggable.draggable( 'option', 'revert', false );
				this.todoEdit = dragmodel.get({ id: id });
				this.todoEdit.save({ status: status });
           		this.todoEdit.set({ status: app.stats[status] });
           		app.todos.fetch({ add : true });
            }


        });
    	return this;
    }
});