app.models.ToDo = Backbone.Model.extend({
    url: function() {
        return "/task" + (this.has("id") ? "/" + this.get("id") : "");
    },
    defaults: {
    	id: null,
        title: null,
        description: null,
        status: 0,
        error: false
    }

});