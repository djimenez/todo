var app = (function() {

	var api = {
		views: {},
		models: {},
		collections: {},
		content: null,
		router: null,
		todos: null,
		stats : ['Open','in_progress', 'Fixed', 'Verified'],
		init: function() {
			this.content = $("#content");
			ViewsFactory.menu();
			this.todos = new api.collections.ToDos();
			this.todos.fetch();
			Backbone.history.start();
			return this;
		},
		changeContent: function(el) {
			this.content.prepend(el);
			return this;
		},
		title: function(str) {
			$("h1").text(str);
			return this;
		},
		errors:function(data){
		    var errorsMessage = '';
		    _.each(data.get('message'),function(element,index){
		        errorsMessage+= '<div class="alert alert-danger"><a class="close" data-dismiss="alert">×</a>'+  
		                            index+' '+ element+  
		                        '</div>  '
		    });
		    $('.error-task').html(errorsMessage);
		    return this;
		}
	};



	var ViewsFactory = {
		menu: function() {
			if(!this.menuView) {
				this.menuView = new api.views.menu({ 
					el: $("#menu") 
				});
			}
			return this.menuView;
		},
		list: function() {
			if(!this.listView) {
				this.listView = new api.views.list({
					model: api.todos
				});
			}	
			return this.listView;
		},
		form: function() {
			if(!this.formView) {
				this.formView = new api.views.form({
					model: api.todos
				}).on("completed", function() {
					this.listView = new api.views.list({
						model: api.todos
					});
					api.router.navigate("", {trigger: true});
				})
			}
			return this.formView;
		}
	};

	var Router = Backbone.Router.extend({
		routes: {
			"new": "newToDo",
			"edit/:index": "editToDo",
			"delete/:id": "delteToDo",
			"": "list"
		},
		list: function(archive) {
			var view = ViewsFactory.list();
			api
			.title("Tasks:")
			.changeContent(view.$el);
		},
		newToDo: function() {
			var view = ViewsFactory.form();
			api.title("New Task:").changeContent(view.$el);
			view.render()
		},
		editToDo: function(id) {
			var view = ViewsFactory.form();
			api.title("Edit Task:").changeContent(view.$el);
			view.render(id);
		},
		delteToDo: function(id) {
			this.todo = app.todos; 
			var newTask = new app.models.ToDo;
			newTask.set({ id:id });
    		newTask.destroy();
    		this.todo.remove(newTask);
    		api.todos.fetch({ remove:true, add : true });
			$('.wrap-form').remove();
			api.router.navigate("", {trigger: true});
		}
	});
	api.router = new Router();

	return api;

})();

window.onload = function() {
    app.init();

}