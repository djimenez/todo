class TaskController < ApplicationController
  protect_from_forgery with: :null_session 
  respond_to :json , :html
  def index
    @task = Tasks.all
    respond_with  @task ,:except => :updated_at
  end

  def create
  	@task = Tasks.new(task_params)
  	if @task.save
  		render :json => {:error => "false"}
  	else
  		render :json => {:error => "true", :message => @task.errors}
  	end
  end

  def destroy
  	@task = Tasks.find(params[:id])
  	if @task.destroy
		render :json => {:error => "false"}
  	else
  		render :json => {:error => "true", :message => @task.errors}
  	end
  end

  def update
    @task = Tasks.find(params[:id])
    if @task.update(task_params)
      render :json => {:error => "false"}
    else
      render :json => {:error => "true", :message => @task.errors}
    end
  end



private
  def task_params
    params.require(:task).permit(:title, :description, :status)
  end

end
