# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Tasks.delete_all
Tasks.create!( id: 1, title: 'prueba1', description: 'prueba desc',  status: 0)
Tasks.create!( id: 2, title: 'prueba12', description: 'prueba desc',  status: 1)
Tasks.create!( id: 3, title: 'prueba13', description: 'prueba desc',  status: 2)
Tasks.create!( id: 4, title: 'prueba14', description: 'prueba desc',  status: 3)
Tasks.create!( id: 5, title: 'prueba16', description: 'prueba desc',  status: 0)
Tasks.create!( id: 6, title: 'prueba17', description: 'prueba desc',  status: 1)
Tasks.create!( id: 7, title: 'prueba18', description: 'prueba desc',  status: 2)
Tasks.create!( id: 8, title: 'prueba19', description: 'prueba desc',  status: 3)
Tasks.create!( id: 9, title: 'prueba10', description: 'prueba desc',  status: 0)
Tasks.create!( id: 10, title: 'prueba1q', description: 'prueba desc',  status: 1)
Tasks.create!( id: 11, title: 'prueba1w', description: 'prueba desc',  status: 2)
Tasks.create!( id: 12, title: 'prueba1e', description: 'prueba desc',  status: 3)

